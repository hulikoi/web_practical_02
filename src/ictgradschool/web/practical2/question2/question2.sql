DROP TABLE IF EXISTS practical_two_files;
CREATE TABLE IF NOT EXISTS practical_two_files (
  file_path VARCHAR(10) NOT NULL,
  file_name VARCHAR(64) NOT NULL,
  file_title VARCHAR(64) NOT NULL,
  file_description TEXT NOT NULL,
  PRIMARY KEY (file_path)
);

INSERT INTO practical_two_files VALUES
  ('ace313.pdf', 'web_lecture_01', 'HTML & Git', 'The first web lecture of the PGCertIT programming introducing students to the basics of HTML and Git.'),
  ('daca76.pdf', 'web_lecture_02', 'HTML & Forms', 'A continuation of the first web lecture, this presentation investigates more HTML features.'),
  ('foo007.pdf', 'web_lecture_03', 'CSS1', 'Introducing Cascading Style Sheets.'),
  ('herp69.pdf', 'web_lecture_04', 'CSS2', 'Getting started with CSS positioning.');

  SELECT * FROM practical_two_files;