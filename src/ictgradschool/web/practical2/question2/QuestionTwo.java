package ictgradschool.web.practical2.question2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;

public class QuestionTwo extends HttpServlet {
    private String pdfDirname;
    private String pdfIcon;


    public void init() {
        // TODO: Retrieve init parameters for the pdf file location and the pdf icon file
        pdfDirname = "pdf-files";
//        pdfDirname = getServletContext().getRealPath("pdf-files");
        pdfIcon = "icon12.png";
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO: Get a list of `FileEntry`s from your DAO
        FileEntryDAO dao = new FileEntryDAO();

        List<FileEntry> files = dao.getAllFileEntriesSorted();

        // TODO: Pass list of `FileEntry`s, pdf file location and pdf icon name to the JSP file
        request.setAttribute("FileEntries",files);
        request.setAttribute("pdfFilePath",pdfDirname);
        request.setAttribute("pdfIcon",pdfIcon);


        // TODO: Pass control to the JSP file
        request.getRequestDispatcher("question2_render.jsp").forward(request,response);
    }
}
