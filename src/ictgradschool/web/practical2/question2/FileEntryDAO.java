package ictgradschool.web.practical2.question2;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class FileEntryDAO {
    // TODO: Hardcode your database connection information here. Yes, this is bad practice, but we forgive you
    private static String url = "jdbc:mariadb://db.sporadic.nz/dwc1";
    private static String user = "dwc1";
    private static String pass = "PestBurnLoveCertainly";



    public static List<FileEntry> getAllFileEntriesSorted() {
        List<FileEntry> entries = new ArrayList<>();
        //nice to do, fin if time.
//        Properties dbProps = new Properties();
//        dbProps.setProperty("")
// TODO: Connect to your database and select all entries from the `practical_two_files` table
        // TODO: Convert each retrieved row into a FileEntry object and add these to the `entries` list
        // TODO: Sort the collection prior to returning it
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException e) { }

        try (Connection conn=DriverManager.getConnection(url,user,pass)){
            try(Statement stmt = conn.createStatement()){
                try(ResultSet rs=stmt.executeQuery("SELECT * FROM practical_two_files")){

                    while(rs.next()){
                        FileEntry file = new FileEntry();

                        file.setFilePath(rs.getString(1));
                        file.setFileName(rs.getString(2));
                        file.setFileTitle(rs.getString(3));
                        file.setFileDesc(rs.getString(4));

                        entries.add(file);
                    }
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Collections.sort(entries);



        return entries;
    }
}
