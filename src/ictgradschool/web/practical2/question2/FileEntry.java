package ictgradschool.web.practical2.question2;

import java.io.Serializable;

public class FileEntry implements Serializable, Comparable<FileEntry> {
    // TODO: Complete this JavaBean so that it can represent a row in the `practical_two_files` table
    private String fileName,filePath,fileTitle,fileDesc;

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileTitle() {
        return fileTitle;
    }

    public void setFileTitle(String fileTitle) {
        this.fileTitle = fileTitle;
    }

    public String getFileDesc() {
        return fileDesc;
    }

    public void setFileDesc(String fileDesc) {
        this.fileDesc = fileDesc;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public int compareTo(FileEntry other) {
        return (this.fileName).compareTo(other.fileName);
    }
}
