-- Complete your SQL here
DROP TABLE IF EXISTS composes;
DROP TABLE IF EXISTS writes;
DROP TABLE IF EXISTS has_genre;
DROP TABLE IF EXISTS has_producer;
DROP TABLE IF EXISTS artists;
DROP TABLE IF EXISTS songs;
DROP TABLE IF EXISTS genres;
DROP TABLE IF EXISTS producers;



CREATE TABLE artists (
  artist_id INT NOT NULL AUTO_INCREMENT,
  lname VARCHAR(32),
  fname VARCHAR(32),
  PRIMARY KEY (artist_id)
);

CREATE TABLE producers (
  producer_id INT NOT NULL AUTO_INCREMENT,
  lname VARCHAR(32),
  fname VARCHAR(32),
  PRIMARY KEY (producer_id)
);

CREATE TABLE songs (
  name VARCHAR(64),
  year INT(4),
  lyrics TEXT,
  producer_id INT,
  PRIMARY KEY (name),
  FOREIGN KEY (producer_id) REFERENCES producers (producer_id)
);

CREATE TABLE genres (
  name VARCHAR(32),
  description TEXT,
  PRIMARY KEY (name)
);

CREATE TABLE composes (
  artist_id INT,
  song_name VARCHAR(64),
  PRIMARY KEY (artist_id,song_name),
  FOREIGN KEY (artist_id) REFERENCES artists (artist_id),
  FOREIGN KEY (song_name) REFERENCES songs (name)
);

CREATE TABLE writes (
  artist_id INT,
  song_name VARCHAR(64),
  PRIMARY KEY (artist_id,song_name),
  FOREIGN KEY (artist_id) REFERENCES artists (artist_id),
  FOREIGN KEY (song_name) REFERENCES songs (name)
);

CREATE TABLE has_genre (
  genre_name VARCHAR(32),
  song_name VARCHAR(64),
  PRIMARY KEY (genre_name,song_name),
  FOREIGN KEY (genre_name) REFERENCES genres (name),
  FOREIGN KEY (song_name) REFERENCES songs (name)
);

CREATE TABLE has_producer (
  producer_id INT,
  song_name VARCHAR(64),
  PRIMARY KEY (producer_id,song_name),
  FOREIGN KEY (producer_id) REFERENCES producers (producer_id),
  FOREIGN KEY (song_name) REFERENCES songs (name)
);


INSERT INTO artists (lname, fname) VALUES
                                          ('may','alicia'),
                                          ('','prince'),
                                          ('','sting'),
                                          ('grant','jody');

INSERT INTO producers (lname, fname) VALUES
                                                                                                       ('rock','kid'),
                                                                                                       ('scorsesi','marvin'),
                                                                                                       ('guy','famous');

INSERT INTO songs (name, year, lyrics, producer_id) VALUES
                                                           ('red corvet',1672,'red cadilac isn\'t as good as my red corbet',1),
                                                           ('song title',1972,'lyrics that are good',2),
                                                           ('my my my',1672,'my my my',1),
                                                           ('eminem me',1672,'I donno',3);

INSERT INTO genres (name, description) VALUES
                                              ('country','hot right now'),
                                              ('rock','rock out'),
                                              ('pop','is popular by definition yet everyone says they hate it'),
                                              ('horror','really shouldnt be in this list');



INSERT INTO composes (artist_id, song_name) VALUES
                                                   (1,'red corvet'),
                                                   (1,'my my my'),
                                                   (2,'song title'),
                                                   (3,'eminem me'),
                                                   (4,'song title');

INSERT INTO writes (artist_id, song_name) VALUES
                                                 (1,'red corvet'),
                                                 (1,'my my my'),
                                                 (2,'song title'),
                                                 (3,'eminem me'),
                                                 (4,'song title');

INSERT INTO has_genre (genre_name, song_name) VALUES
                                                     ('country','red corvet'),
                                                     ('horror','song title'),
                                                     ('pop','my my my'),
                                                     ('rock','eminem me');

INSERT INTO has_producer (producer_id, song_name) VALUES
                                                         (1,'red corvet'),
                                                         (2,'song title'),
                                                         (3,'my my my'),
                                                         (1,'eminem me');





