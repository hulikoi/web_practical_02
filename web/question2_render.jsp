<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="jstl" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Question 2 - File List</title>

    <style type="text/css">
        /* You do not need to change these styles */
        a, dl {
            float: left;
        }

        /*add tooltip hover stuff*/
        .tooltip {
            position: relative;
            display: inline-block;
        }

        /*.tooltip*/
        .tooltiptext {
            visibility: hidden;
            width: 120px;
            background-color: black;
            color: #fff;
            text-align: center;
            padding: 5px 0;
            border-radius: 6px;
            position: absolute;
            z-index: 1;
        }

        .tooltip:hover .tooltiptext {
            visibility: visible;
        }

        div::after {
            content: "";
            clear: both;
            display: table;
        }

        img {
            visibility: visible;
        }
    </style>
</head>
<body>
<!-- TODO: Display a header showing which folder we are displaying files from -->
<h1>List of files from the ${pdfFilePath} directory</h1>
<!-- TODO: Display each provided FileEntry -->
<jstl:forEach items="${FileEntries}" var="file">
    <div>
        <%--<span class="tooltip">--%>

            <a href="./${pdfFilePath}/${file.filePath}" class="tooltip" target="_blank" download=""><span class="tooltiptext">${file.fileName}</span><img  src="./${pdfFilePath}/${pdfIcon}"></a>
            <%--</span>--%>
            <%--<div>--%>

        <dl>
            <dt>
                    ${file.fileName}
            </dt>
            <dd>
                    ${file.fileDesc}
            </dd>
        </dl>

            <%--</div>--%>
    </div>
</jstl:forEach>
</body>
</html>
